#pragma once

#include <Uefi.h>

#include <boot/base.h>

typedef struct {
    UINT8   signature[8];
    UINT32  revision;
    UINT32  header_size;
    UINT32  header_crc32;
    UINT32  reserved;
    UINT64  current_lba;
    UINT64  alternate_lba;
    UINT64  first_usable_lba;
    UINT64  last_usable_lba;
    UINT8   disk_guid[16];
    UINT64  partition_entry_lba;
    UINT32  num_partition_entries;
    UINT32  partition_entry_size;
    UINT32  partition_entry_array_crc32;
} __attribute__((packed)) gpt_header;

const UINT8 GPT_HEADER_SIGNATURE[] = {0x45, 0x46, 0x49, 0x20, 0x50, 0x41, 0x52, 0x54}; // "EFI PART"

typedef struct {
    UINT8   partition_type_guid[16];
    UINT8   unique_partition_guid[16];
    UINT64  starting_lba;
    UINT64  ending_lba;
    UINT64  attributes;
    UINT16  partition_name[36];
} __attribute__((packed)) gpt_entry;

#define EFI_SYSTEM_PARTITION_GUID {0xC12A7328, 0xF81F, 0x11D2, {0xBA, 0x4B, 0x00, 0xA0, 0xC9, 0x3E, 0xC9, 0x3B}}    // "C12A7328-F81F-11D2-BA4B-00A0C93EC93B"
#define LINUX_FILESYSTEM_GUID     {0x0FC63DAF, 0x8483, 0x4772, {0x8E, 0x79, 0x3D, 0x69, 0xD8, 0x47, 0x7D, 0xE4}}    // "0FC63DAF-8483-4772-8E79-3D69D8477DE4"
                                                                                                                     
static void gpt_printhead(EFI_SYSTEM_TABLE* systemTable, gpt_header head) {
    
    hexdump((void*) &head, 0, 64);
    
    print("GPT Header:"); newline();
    print("    header size: "); itoa(head.header_size, 10); newline();
    print("    current header block: "); itoa(head.current_lba, 10); newline();
    print("    first usable block: "); itoa(head.first_usable_lba, 10); newline();
    print("    partition count: "); itoa(head.num_partition_entries, 10); newline();
    print("    partition entry size: "); itoa(head.partition_entry_size, 10); newline();

    UINTN size = (head.alternate_lba + 1) * 512; // TODO: dont assume 512 byte blocks
    int b = 0;
    while(size >= 1024) {
        size /= 1024; b++;
    }

    print("    disk size: "); itoa(size, 10); switch (b) { // print the size in a human readable format
        case 0: print("b"); break;
        case 1: print("Kib"); break;
        case 2: print("Mib"); break;
        case 3: print("Gib"); break;
        case 4: print("Tib"); break;
        case 5: print("Pib"); break;
        case 6: print("Eib"); break;

        default: print("???"); break; // at this point it doesnt matter, doubt anyone will have a disk this big whilst supporting uefi
    } newline();

}

// prints a gpt partition entry, if index is -1, it will not print the index
static void gpt_printentry(EFI_SYSTEM_TABLE* systemTable, gpt_entry entry, UINT8 index) { 
    print("GPT Entry: "); if(index + 1) itoa(index, 10); newline();
    print("    type: "); printGuid(systemTable, *(EFI_GUID*) &entry.partition_type_guid); newline();
    print("    starting block: "); itoa(entry.starting_lba, 10); newline();
    print("    ending block: "); itoa(entry.ending_lba, 10); newline();
}