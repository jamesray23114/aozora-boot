#pragma once 

#include <Uefi.h>
#include <Protocol/BlockIo.h>
#include <Protocol/DiskIo.h>

#include <boot/base.h>
#include <boot/fs/gpt.h>


// returns an array of all connected disk controllers, they may not be active
static EFI_BLOCK_IO_PROTOCOL** get_disks(EFI_SYSTEM_TABLE* systemTable, UINTN* count) {

    EFI_STATUS status;
    EFI_HANDLE* handles;

    systemTable->BootServices->LocateHandleBuffer(ByProtocol, & (EFI_GUID) EFI_BLOCK_IO_PROTOCOL_GUID, NULL, count, &handles);
    
    EFI_BLOCK_IO_PROTOCOL** diskio; 
    systemTable->BootServices->AllocatePool(EfiLoaderData, sizeof(EFI_BLOCK_IO_PROTOCOL*) * *count, (void**) &diskio);

    for (UINTN i = 0; i < *count; i++) {
        status = systemTable->BootServices->HandleProtocol(handles[i], & (EFI_GUID) EFI_BLOCK_IO_PROTOCOL_GUID, (void**) &diskio[i]);
        if (status) print("error getting diskio protocol\n");
    }

    return diskio;
}

static EFI_BLOCK_IO_PROTOCOL* find_boot_disk(EFI_SYSTEM_TABLE* systemTable, EFI_BLOCK_IO_PROTOCOL** disklist, UINTN count) {

    for(int i = 0; i < count; i++) {
        int lbasize = disklist[i]->Media->BlockSize;
        UINT8* buffer;

        systemTable->BootServices->AllocatePool(EfiLoaderData, lbasize, (void**) &buffer);
        disklist[i]->ReadBlocks(disklist[i], disklist[i]->Media->MediaId, 1, lbasize, buffer);

        // look for a GPT partition table. TODO: ensure this works on systems with multiple disks, currently we assume UEFI will only load the booted disk
        for(int j = 0; j < 8; j++) {
            if (buffer[j] != "EFI PART"[j]) break;            
            if (j == 7) return disklist[i];
        }
    }

    return NULL;
}

// start and size are in bytes, allows for non sector aligned reads
// TODO: buffering, currently we read from the disk every time
static void disk_read(EFI_SYSTEM_TABLE* systemTable, EFI_BLOCK_IO_PROTOCOL* disk, UINTN start, UINTN size, UINT8* buffer) {
    UINTN lbasize = disk->Media->BlockSize;
    UINTN startlba = start / lbasize;
    UINTN endlba = (start + size) / lbasize;
    UINTN readsize = endlba - startlba + 1;

    UINT8* tbuffer;
    systemTable->BootServices->AllocatePool(EfiLoaderData, readsize * lbasize, (void**) &tbuffer);

    if(disk->ReadBlocks(disk, disk->Media->MediaId, startlba, readsize * lbasize, tbuffer)) return; 
    
    for (int i = 0; i < size; i++) ((UINT8*) buffer)[i] = ((UINT8*) tbuffer)[i + (start % lbasize)];
    
    systemTable->BootServices->FreePool(tbuffer);
}

// start and size are in bytes, allows for non sector aligned writes
// TODO: buffering, currently we must read the entire sector, modify it, and write it back
static void disk_write(EFI_SYSTEM_TABLE* systemTable, EFI_BLOCK_IO_PROTOCOL* disk, UINTN start, UINTN size, void* buffer) {
    UINTN lbasize = disk->Media->BlockSize;
    UINTN startlba = start / lbasize;
    UINTN endlba = (start + size) / lbasize;
    UINTN readsize = endlba - startlba + 1;

    void* tbuffer;

    systemTable->BootServices->AllocatePool(EfiLoaderData, readsize * lbasize, (void**) &tbuffer);
    if(disk->ReadBlocks(disk, disk->Media->MediaId, startlba, readsize * lbasize, tbuffer)) {
        buffer = NULL;
        return;
    }

    for (int i = 0; i < size; i++) ((UINT8*) tbuffer)[i + (start % lbasize)] = ((UINT8*) buffer)[i];

    disk->WriteBlocks(disk, disk->Media->MediaId, startlba, readsize * lbasize, tbuffer);
    disk->FlushBlocks(disk);

    systemTable->BootServices->FreePool(tbuffer);
}