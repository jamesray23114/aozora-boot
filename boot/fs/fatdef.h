#pragma once

#include <Uefi.h>
#include <Protocol/BlockIo.h>
#include <Protocol/DiskIo.h>

typedef struct 
{
    /* 0x00 */ UINT8    jmp[3];
    /* 0x30 */ UINT8    oem[8];
    /* 0xB0 */ UINT16   bytes_per_sector;
    /* 0xD0 */ UINT8    sectors_per_cluster;
    /* 0xE0 */ UINT16   reserved_sectors;
    /* 0x10 */ UINT8    fat_count;
    /* 0x11 */ UINT16   root_max_entries;
    /* 0x13 */ UINT16   total_sectors;
    /* 0x15 */ UINT8    media_descriptor;
    /* 0x16 */ UINT16   sectors_per_fat;
    /* 0x18 */ UINT16   sectors_per_track;
    /* 0x1A */ UINT16   head_count;
    /* 0x1C */ UINT32   hidden_sectors;
    /* 0x20 */ UINT32   total_sectors_32;
    /* 0x24 */ UINT32   sectors_per_fat_32;
    /* 0x28 */ UINT16   fat_flags;
    /* 0x2A */ UINT16   fat_version;
    /* 0x2C */ UINT32   root_cluster;
    /* 0x30 */ UINT16   fsinfo_sector;
    /* 0x32 */ UINT16   backup_boot_sector;
    /* 0x34 */ UINT8    reserved1[12];
    /* 0x40 */ UINT8    drive_number;
    /* 0x41 */ UINT8    reserved2;
    /* 0x42 */ UINT8    boot_signature;
    /* 0x43 */ UINT32   volume_id;
    /* 0x47 */ UINT8    volume_label[11];
    /* 0x52 */ UINT8    fat_type_label[8];
} __attribute__((packed)) fat32_boot;

typedef struct  
{
    UINTN   fat32_boot_lba;
} fat32fs;


typedef struct  {
    /* 0x00 */ UINT8    name[8];
    /* 0x08 */ UINT8    ext[3];
    /* 0x0B */ UINT8    attributes;
    /* 0x0C */ UINT8    reserved;
    /* 0x0D */ UINT8    create_time_ms;
    /* 0x0E */ UINT16   create_time;
    /* 0x10 */ UINT16   create_date;
    /* 0x12 */ UINT16   access_date;
    /* 0x14 */ UINT16   cluster_high;
    /* 0x16 */ UINT16   modify_time;
    /* 0x18 */ UINT16   modify_date;
    /* 0x1A */ UINT16   cluster_low;
    /* 0x1C */ UINT32   size;
} __attribute__((packed)) fat32_direntry;

#define FAT32_ATTR_READ_ONLY    0x01
#define FAT32_ATTR_HIDDEN       0x02
#define FAT32_ATTR_SYSTEM       0x04
#define FAT32_ATTR_VOLUME_ID    0x08
#define FAT32_ATTR_DIRECTORY    0x10
#define FAT32_ATTR_ARCHIVE      0x20
#define FAT32_ATTR_LONG_NAME    0x0F

#define ISLONGNAME(x)           ((x) == FAT32_ATTR_LONG_NAME)

typedef struct {
    /* 0x00 */ UINT8    sequence;
    /* 0x01 */ UINT16   name1[5];
    /* 0x0B */ UINT8    attributes;
    /* 0x0C */ UINT8    type;
    /* 0x0D */ UINT8    checksum;
    /* 0x0E */ UINT16   name2[6];
    /* 0x1A */ UINT16   reserved;
    /* 0x1C */ UINT16   name3[2];
} __attribute__((packed)) fat32_longname;

#define FAT32_LONGNAME_LAST     0x40
#define FAT32_LONGNAME_ATTR     0x0F
#define FAT32_LONGNAME_TYPE     0x00
#define FAT32_LONGNAME_DEL      0xE5

typedef struct {
    /* 0x00 */ UINT32   lead_signature;
    /* 0x04 */ UINT8    reserved1[480];
    /* 0x1E4 */ UINT32  struct_signature;
    /* 0x1E8 */ UINT32  free_cluster_count;
    /* 0x1EC */ UINT32  next_free_cluster;
    /* 0x1F0 */ UINT8   reserved2[12];
    /* 0x1FC */ UINT32  trail_signature;
} __attribute__((packed)) fat32_info;

#define FAT32_LEAD_SIGNATURE    0x41`61`52`52
#define FAT32_STRUCT_SIGNATURE  0x61`41`72`72