#include <Uefi.h>
#include <Guid/Acpi.h>
#include <Protocol/GraphicsOutput.h>
#include <Protocol/DevicePath.h>

#include <Protocol/PartitionInfo.h>

#include <boot/base.h>
#include <boot/config/entries.h>
#include <boot/fs/fat.h>
#include <boot/fs/disk.h>


EFI_STATUS efi_main(EFI_HANDLE* imageHandle, EFI_SYSTEM_TABLE* systemTable) {

    systemTable->BootServices->SetWatchdogTimer(0, 0, 0, NULL);    
    systemTable->ConOut->ClearScreen(systemTable->ConOut);

    // fetch rsdp

    EFI_ACPI_1_0_ROOT_SYSTEM_DESCRIPTION_POINTER* rsdp1 = NULL;
    EFI_ACPI_2_0_ROOT_SYSTEM_DESCRIPTION_POINTER* rsdp2 = NULL;

    for (int i = 0; i < systemTable->NumberOfTableEntries; i++) {
        if (guid_equals(systemTable->ConfigurationTable[i].VendorGuid, (EFI_GUID) ACPI_10_TABLE_GUID)) {
            rsdp1 = systemTable->ConfigurationTable[i].VendorTable;
            break;
        } 
        if (guid_equals(systemTable->ConfigurationTable[i].VendorGuid, (EFI_GUID) EFI_ACPI_20_TABLE_GUID)) {
            rsdp2 = systemTable->ConfigurationTable[i].VendorTable;
            break;
        }
    }
    
    if (rsdp2 == NULL && rsdp1 == NULL) { // exit if no rsdp found
        exitin(imageHandle, systemTable, L"No RSDP found");
    }

    UINTN count;

    EFI_BLOCK_IO_PROTOCOL** disks = get_disks(systemTable, &count);
    EFI_BLOCK_IO_PROTOCOL* boot_disk = find_boot_disk(systemTable, disks, count);

    if (boot_disk == NULL) exitin(imageHandle, systemTable, L"No boot disk found");
    
    char* str1 = "hello world";
    char* str2 = "test string";

    disk_write(systemTable, boot_disk, 0x2000000, 11, str1);
    disk_write(systemTable, boot_disk, 0x2000009, 11, str2);

    // load fat32 filesystem driver


    // fetch config 

    // fetch OS image

    // load OS image

    // fetch memory map

    // exit boot services

    // jump to OS image

    while (1) ;
}