#pragma once

#include <Uefi.h>

static CHAR16* __tempstr;

#define printfound(ptr) if (ptr) systemTable->ConOut->OutputString(systemTable->ConOut, #ptr L" found \r\n"); else systemTable->ConOut->OutputString(systemTable->ConOut, #ptr L"not found\r\n")
#define newline() systemTable->ConOut->OutputString(systemTable->ConOut, (CHAR16*) L"\r\n")
#define print(str) systemTable->ConOut->OutputString(systemTable->ConOut, (CHAR16*) L"" str)
#define itoa(value, base) __tempstr = _itoa(systemTable->BootServices, value, base);  systemTable->ConOut->OutputString(systemTable->ConOut, __tempstr); systemTable->BootServices->FreePool(__tempstr)
#define hexdump(ptr, start, size) _hexdump(systemTable, ptr, start, size)

static inline BOOLEAN guid_equals(EFI_GUID guid1, EFI_GUID guid2) {
    return guid1.Data1 == guid2.Data1 &&
           guid1.Data2 == guid2.Data2 &&
           guid1.Data3 == guid2.Data3 &&
           guid1.Data4[0] == guid2.Data4[0] &&
           guid1.Data4[1] == guid2.Data4[1] &&
           guid1.Data4[2] == guid2.Data4[2] &&
           guid1.Data4[3] == guid2.Data4[3] &&
           guid1.Data4[4] == guid2.Data4[4] &&
           guid1.Data4[5] == guid2.Data4[5] &&
           guid1.Data4[6] == guid2.Data4[6] &&
           guid1.Data4[7] == guid2.Data4[7];
}

static inline CHAR16* _itoa(EFI_BOOT_SERVICES* alloc, UINTN value, UINTN base) { 
    UINTN length = 0;
    UINTN temp = value;

    if (value == 0) {
        CHAR16* out = NULL;
        alloc->AllocatePool(EfiLoaderData, 2 * sizeof(CHAR16), (void**) &out);
        out[0] = '0';
        out[1] = 0;
        return out;
    }

    while (temp > 0) {
        temp /= base;
        length++;
    }

    CHAR16* out = NULL;
    alloc->AllocatePool(EfiLoaderData, length * sizeof(CHAR16), (void**) &out);

    out[length] = 0;
    temp = value;
    while (temp > 0) {
        int toadd = temp % base > 9 ? 'A' - 10 : '0';
        out[--length] = (temp % base) + toadd;
        temp /= base;
    }
    return out;
}

static void _hexdump(EFI_SYSTEM_TABLE* systemTable, void* ptr, UINTN start, UINTN size) {
    UINTN padding = 0;
    UINTN total = start + size;
    while( total /= 16) padding++;

    for (int i = 0; i < size; i++) {
        if (i % 16 == 0) {
            newline();
            
            int temp = start + i;
            int ptemp = padding;
            while (temp /= 16) ptemp--;
            while (ptemp--) print("0");

            itoa(start + i, 16);
            print(" -> ");
            
            temp = start + i + 15;
            ptemp = padding;
            while (temp /= 16) ptemp--;
            while (ptemp--) print("0");
            
            itoa(start + i + 15, 16);
            print(": ");
        } else if (i % 8 == 0) {
            print("| ");
        }

        itoa(((UINT8*) ptr)[i], 16);
        if (((UINT8*) ptr)[i] < 0x10) print("0");

        print(" ");
    }
    newline();
}

static void exitin(EFI_HANDLE imageHandle, EFI_SYSTEM_TABLE* systemTable, CHAR16* string) {
        systemTable->ConOut->OutputString(systemTable->ConOut, string); newline();
        print("5...\r");
        systemTable->BootServices->Stall(1000000);
        print("4...\r");
        systemTable->BootServices->Stall(1000000);
        print("3...\r");
        systemTable->BootServices->Stall(1000000);
        print("2...\r");
        systemTable->BootServices->Stall(1000000);
        print("1...\r");
        systemTable->BootServices->Stall(1000000);
        systemTable->BootServices->Exit(imageHandle, EFI_NOT_FOUND, 0, NULL);
}

static void printGuid(EFI_SYSTEM_TABLE* systemTable, EFI_GUID guid) {
    itoa(guid.Data1, 16); print("-");
    itoa(guid.Data2, 16); print("-");
    itoa(guid.Data3, 16); print("-");
    itoa(guid.Data4[0], 16); itoa(guid.Data4[1], 16); print("-");
    itoa(guid.Data4[2], 16); itoa(guid.Data4[3], 16); itoa(guid.Data4[4], 16); itoa(guid.Data4[5], 16); itoa(guid.Data4[6], 16); itoa(guid.Data4[7], 16);
}