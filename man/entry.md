entries are placed in /EFI/config/entry8 or /EFI/config/entry16, depending on the text encoding (8-bit or 16-bit).\
entries have the file extension .entry\
if a entry is named nomenu.entry, it will boot by default.

## Usage
```
any instance of '//' is a comment and will cause the parser to ignore the rest of the line

// the following must go first, an unexpected entry will cause the parser to exit
(optional) name "name of entry"     // this will by default be the name of the file, this cannot use non utf8 characters and excludes some characters TBA
device dxpy                         // this is the device where the file to be loaded is located in the format dxpy, where x is the drive number and y is the partition number
partformat "format"                 // this is the format of the partition, this is used to determine the filesystem driver to use, as of version 0.1.0 this is required, but in the future it will be optional
                                    // supported filesystems: fat32 (as of v0.1.0)    

// these can be repated as many times as needed
module "name"                       // this will load the named module
                                    // supported modules: gop (as of v0.1.0)

// one of the following must be present
cede "path"                         // this will give up control to the file at "path", must be a .efi file 
image "path"                        // the kernel image 

```

## example
```
// windows

name "windows"
device d0p1
partformat "fat32"
cede "efi/microsoft/boot/bootmgfw.efi"
```

## todo
```
sub-menu support via directories
modules and linux support (either directly or via another bootloader)
more filesystems
```