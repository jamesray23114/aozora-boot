.SILENT:
.PHONY: build
.PHONY: clean
.PHONY: run
.PHONY: bootimg
.PHONY: all

PATH_TO_OVMF :=/usr/share/ovmf/x64/OVMF.fd
PATH_TO_BOOTLOADER :=.temp/BOOTX64.EFI

CFLAGS := -ffreestanding -flto -fno-stack-protector -fshort-wchar -finline-limit=1024 -mno-red-zone -I . -I uefi-headers/Include -I uefi-headers/Include/X64 -Ofast -mno-ms-bitfields 
LFLAGS := -nostdlib -flto -Wl,-dll -shared -Wl,--subsystem,10 -e efi_main -Ofast

QEMUFLAGS := -machine q35,accel=kvm -cpu host -smp 4 -m 4G -no-reboot -usb -vga std -device virtio-keyboard-pci
QEMUBIOS  := -drive format=raw,if=pflash,readonly=on,file=$(PATH_TO_OVMF)
QEMUDISK  := -drive format=raw,if=ide,file=.temp/hdd.img

build: boot/main.c
	mkdir -p .temp 
	x86_64-w64-mingw32-gcc $(CFLAGS) -c boot/main.c -o .temp/main.o
	x86_64-w64-mingw32-gcc $(LFLAGS) .temp/main.o -o $(PATH_TO_BOOTLOADER)

clean:
	rm -rf .temp

bootimg: build
	truncate -s 32M 	.temp/boot.img
	truncate -s 1024M 	.temp/hdd.img

	mkfs.fat -v -F 32 -S 512 -s 1 .temp/boot.img 65535 1> /dev/null 2> /dev/null
	mmd -i .temp/boot.img ::/EFI
	mmd -i .temp/boot.img ::/EFI/BOOT
	mcopy -i .temp/boot.img $(PATH_TO_BOOTLOADER) ::/EFI/BOOT/

	mmd -i .temp/boot.img ::/EFI/config
	mmd -i .temp/boot.img ::/EFI/config/entry8
	mmd -i .temp/boot.img ::/EFI/config/entry16

	mcopy -i .temp/boot.img test/nomenu16.entry ::/EFI/config/entry16/nomenu.entry
	mcopy -i .temp/boot.img test/nomenu8.entry ::/EFI/config/entry8/nomenu.entry


	parted -s -a minimal .temp/hdd.img mklabel gpt
	parted -s -a minimal .temp/hdd.img mkpart EFI FAT32 64s 65600s
	parted -s -a minimal .temp/hdd.img toggle 1 boot

	dd if=.temp/boot.img of=.temp/hdd.img bs=512 count=65535 seek=64 conv=notrunc 1> /dev/null 2> /dev/null

run: 
	qemu-system-x86_64 $(QEMUFLAGS) $(QEMUBIOS) $(QEMUDISK) 

all: bootimg run